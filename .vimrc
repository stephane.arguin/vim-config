
" Enable filetype plugins
filetype plugin on
filetype indent on

"" General
set history=1000   " How many commands to remember
set autoread       " Auto-read file when it's changed from the outside

set linebreak      " Break lines at word (requires Wrap lines)
set showbreak=+++  " Wrap-broken line prefix
set textwidth=80   " Line wrap (number of cols)
set showmatch      " Highlight matching brace
set undolevels=1000 " Number of undo levels

"" Text rendering
set display+=lastline " Always try to show a paragraph’s last line.
set encoding=utf-8    " Use an encoding that supports unicode.
set linebreak         " Avoid wrapping a line in the middle of a word.
set scrolloff=1       " The number of screen lines to keep above and below the cursor.
set sidescrolloff=5   " The number of screen columns to keep to the left and right of the cursor.
syntax enable         " Enable syntax highlighting.
set wrap              " Enable line wrapping.

"" Indentation
set autoindent         " Auto-indent new lines
set expandtab          " Use spaces instead of tabs
set shiftwidth=2       " Number of auto-indent spaces
set smartindent        " Enable smart-indent
set smarttab           " Enable smart-tabs
set softtabstop=2      " Number of spaces per Tab
set tabstop=2          " Tab length

"" Search
set hlsearch	   " Highlight all search results
set smartcase	   " Enable smart-case search
set ignorecase   " Always case-insensitive
set incsearch    " Searches for strings incrementally

"" User interface
if (&term =~ "xterm") || (&term =~ "screen")
  set t_Co=256
endif

colorscheme fahrenheit
set laststatus=2       " Always display the status bar.
set ruler              " Show row and column ruler information
set tabpagemax=50      " Maximum number of tab pages that can be opened from the command line.
set cursorline         " Highlight the line currently under cursor.
set number             " Show line numbers
set noerrorbells       " Disable beep on errors.
set visualbell         " Use visual bell (no beeping)
set mouse=a            " Enable mouse for scrolling and resizing.
set title              " Set the window’s title, reflecting the file currently being edited.
set backspace=eol,start,indent  " Backspace behaviour
set showbreak=>\ 
set scrolljump=5                " Jump 5 lines when running out of the screen
set scrolloff=3                 " Jump out of the screen when 3 lines before end of the screen

set wildmenu              " Display command line’s tab complete options as a menu.
set wildmode=longest,list " make completion more like Bash
set wildignore=*.o,*~,*.pyc,*.bak,*.swp,*/.git/*,*/.svn/*

"" Performance
set lazyredraw     " Don’t update screen during macro and script execution.

"" Files, backups, ...
set autochdir                   " Change working directory to open buffer
set backupdir=~/.vim/backup     " Place to store backup files (*~ files)
set backup                      " Enable backup files
set fileencoding=utf-8          " UTF-8 encoding
set fileformats=unix,dos,mac    " Default to Unix file format (eol is LF)

"" Commands/keys mapping
command W w !sudo tee % > /dev/null   " Sudo save file (in case of permission issue)

let mapleader=","
let g:mapleader=","
nmap <leader>w :w<cr>
nmap <leader>q :q<cr>

inoremap jj <Esc>
inoremap <leader><Tab> <C-v><Tab>

"" Per filetype 
autocmd BufRead,BufNewFile *.csv,*.log,*.xml,*.json
  \ setlocal textwidth=0 wrapmargin=0

autocmd BufRead,BufNewFile *.tsv,*.tab 
  \ setlocal textwidth=0 wrapmargin=0 noexpandtab
